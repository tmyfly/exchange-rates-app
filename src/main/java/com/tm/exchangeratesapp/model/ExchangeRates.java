package com.tm.exchangeratesapp.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class ExchangeRates{
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;

    @ManyToMany
    private List<RateBank> rateBankList = new ArrayList<>();

    @ManyToMany
    private List<AverageExchangeRates> averageExchangeRatesList = new ArrayList<>();

    private final LocalDateTime time = LocalDateTime.now();
}
