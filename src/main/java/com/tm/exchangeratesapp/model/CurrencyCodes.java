package com.tm.exchangeratesapp.model;

public enum CurrencyCodes {
    EUR(978),
    USD(840),
    UAH	(980);

    CurrencyCodes(int i) {
        this.i = i;
    }

    private int i;

    public int getValue() {
        return i;
    }



}
