package com.tm.exchangeratesapp.controller;

import com.tm.exchangeratesapp.model.AverageExchangeRates;
import com.tm.exchangeratesapp.model.ExchangeRates;
import com.tm.exchangeratesapp.service.AverageExchangeRatesService;
import com.tm.exchangeratesapp.service.ExchangeRatesService;
import io.swagger.annotations.ApiOperation;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("/rates")
public class ExchangeRatesController {
    private final ExchangeRatesService exchangeRatesService;
    private final AverageExchangeRatesService averageExchangeRatesService;

    public ExchangeRatesController(ExchangeRatesService exchangeRatesService,
                                   AverageExchangeRatesService averageExchangeRatesService) {
        this.exchangeRatesService = exchangeRatesService;
        this.averageExchangeRatesService = averageExchangeRatesService;
    }

    @GetMapping("/all")
    public ResponseEntity<List<ExchangeRates>> getAll() {
        return new ResponseEntity<List<ExchangeRates>>(exchangeRatesService.findAll(), HttpStatus.OK);

    }

    @ApiOperation(value = "Returns average exchange rates for all sources for the period", notes = "Returns average exchange rates for all sources for the period.")
    @GetMapping("/period")
    public ResponseEntity<List<AverageExchangeRates>> period() {
        return new ResponseEntity<List<AverageExchangeRates>>(averageExchangeRatesService.getLast(), HttpStatus.OK);

    }

    @ApiOperation(value = "Returns average exchange rates for all sources for the selected period", notes = "Returns average exchange rates for all sources for the selected period. Date pattern = \"yyyy-MM-dd\"")
    @GetMapping(value = "/search-peroid")
    public ResponseEntity<List<AverageExchangeRates>> search(@RequestParam("startDate")
                                                      @DateTimeFormat(pattern = "yyyy-MM-dd") Date startDate,
                                                      @RequestParam("endDate")
                                                      @DateTimeFormat(pattern = "yyyy-MM-dd") Date endDate) {

        var startDateLoc = convertToLocalDateTime(startDate);
        var endDateLoc = convertToLocalDateTime(endDate);

        return new ResponseEntity<List<AverageExchangeRates>>(averageExchangeRatesService.searchByCriteria(startDateLoc, endDateLoc), HttpStatus.OK);
    }

    private LocalDateTime convertToLocalDateTime(Date dateToConvert) {
        return dateToConvert.toInstant()
                .atZone(ZoneId.systemDefault())
                .toLocalDateTime();
    }
}
