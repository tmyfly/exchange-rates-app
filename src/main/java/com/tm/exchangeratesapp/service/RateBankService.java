package com.tm.exchangeratesapp.service;

import com.tm.exchangeratesapp.model.RateBank;

public interface RateBankService {
    RateBank save(RateBank rateBank);
    RateBank readById(long id);
}
