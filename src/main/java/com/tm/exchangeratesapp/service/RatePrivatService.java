package com.tm.exchangeratesapp.service;

import com.tm.exchangeratesapp.model.RatePrivat;

public interface RatePrivatService {
    RatePrivat save(RatePrivat ratePrivat);
    RatePrivat readById(long id);

}
