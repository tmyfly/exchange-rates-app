package com.tm.exchangeratesapp.service;

import com.tm.exchangeratesapp.model.RateNbu;
import com.tm.exchangeratesapp.repository.RateNbuRepository;
import org.springframework.stereotype.Service;

@Service
public class RateNbuServiceImpl implements RateNbuService{
    private final RateNbuRepository repository;

    public RateNbuServiceImpl(RateNbuRepository repository) {
        this.repository = repository;
    }

    @Override
    public RateNbu save(RateNbu rateNbu) {
        return repository.save(rateNbu);
    }

    @Override
    public RateNbu readById(long id) {
        return null;
    }
}
