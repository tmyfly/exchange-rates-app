package com.tm.exchangeratesapp.service;

import com.tm.exchangeratesapp.model.RateNbu;

public interface RateNbuService {
    RateNbu save(RateNbu rateNbu);
    RateNbu readById(long id);

}
