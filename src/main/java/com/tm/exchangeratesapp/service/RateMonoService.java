package com.tm.exchangeratesapp.service;

import com.tm.exchangeratesapp.model.RateMono;

public interface RateMonoService {
    RateMono save(RateMono rateMono);
    RateMono readById(long id);

}
