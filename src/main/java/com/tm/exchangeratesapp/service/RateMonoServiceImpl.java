package com.tm.exchangeratesapp.service;

import com.tm.exchangeratesapp.model.RateMono;
import com.tm.exchangeratesapp.repository.RateMonoRepository;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class RateMonoServiceImpl implements RateMonoService{
    private final RateMonoRepository repository;

    public RateMonoServiceImpl(RateMonoRepository repository) {
        this.repository = repository;
    }

    @Override
    public RateMono save(RateMono rateMono) {
        Optional<RateMono> rateMonoExist = findCurrCode(rateMono);
        return rateMonoExist.orElseGet(() -> repository.save(rateMono));
    }

    @Override
    public RateMono readById(long id) {
        return null;
    }

    private Optional<RateMono> findCurrCode(RateMono rateMono) {
        return repository.findRateMonoByCurrencyCodeAAndCurrencyCodeBAndDate(rateMono.getCurrencyCodeA(),
                rateMono.getCurrencyCodeB(), rateMono.getDate());
    }

}
