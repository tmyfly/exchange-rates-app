package com.tm.exchangeratesapp.service;

import com.tm.exchangeratesapp.model.RatePrivat;
import com.tm.exchangeratesapp.repository.RatePrivatRepository;
import org.springframework.stereotype.Service;

@Service
public class RatePrivatServiceImpl implements RatePrivatService{
    private final RatePrivatRepository repository;

    public RatePrivatServiceImpl(RatePrivatRepository repository) {
        this.repository = repository;
    }

    @Override
    public RatePrivat save(RatePrivat ratePrivat) {
        return repository.save(ratePrivat);
    }

    @Override
    public RatePrivat readById(long id) {
        return null;
    }
}
