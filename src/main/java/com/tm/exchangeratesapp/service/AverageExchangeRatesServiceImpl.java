package com.tm.exchangeratesapp.service;

import com.tm.exchangeratesapp.model.AverageExchangeRates;
import com.tm.exchangeratesapp.repository.AverageExchangeRatesRepository;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

@Service
public class AverageExchangeRatesServiceImpl implements AverageExchangeRatesService{
    private final AverageExchangeRatesRepository repository;
    private final ExchangeRatesService exchangeRatesService;

    public AverageExchangeRatesServiceImpl(AverageExchangeRatesRepository repository, ExchangeRatesService exchangeRatesService) {
        this.repository = repository;
        this.exchangeRatesService = exchangeRatesService;
    }

    @Override
    public AverageExchangeRates save(AverageExchangeRates averageExchangeRates) {
        return repository.save(averageExchangeRates);
    }

    @Override
    public AverageExchangeRates readById(long id) {
        return null;
    }

    @Override
    public List<AverageExchangeRates> findAll() {
        return repository.findAll();
    }

    @Override
    public List<AverageExchangeRates> getLast() {
        return exchangeRatesService.getLast().getAverageExchangeRatesList();
    }

    @Override
    public List<AverageExchangeRates> searchByCriteria(LocalDateTime startDate, LocalDateTime endDate) {
        return repository.getAverageExchangeRatesByTimeBetween(startDate, endDate);
    }
}
