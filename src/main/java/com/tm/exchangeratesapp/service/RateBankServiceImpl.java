package com.tm.exchangeratesapp.service;

import com.tm.exchangeratesapp.model.RateBank;
import com.tm.exchangeratesapp.repository.RateBankRepository;
import org.springframework.stereotype.Service;

@Service
public class RateBankServiceImpl implements RateBankService{
    private final RateBankRepository repository;

    public RateBankServiceImpl(RateBankRepository repository) {
        this.repository = repository;
    }

    @Override
    public RateBank save(RateBank rateBank) {
        return repository.save(rateBank);
    }

    @Override
    public RateBank readById(long id) {
        return null;
    }
}
