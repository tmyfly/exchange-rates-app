package com.tm.exchangeratesapp.service;

import com.tm.exchangeratesapp.model.ExchangeRates;

import java.util.List;

public interface ExchangeRatesService {
    ExchangeRates save(ExchangeRates exchangeRates);
    ExchangeRates readById(long id);
    ExchangeRates getLast();
    List<ExchangeRates> findAll();
}
