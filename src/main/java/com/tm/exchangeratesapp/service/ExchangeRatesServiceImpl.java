package com.tm.exchangeratesapp.service;

import com.tm.exchangeratesapp.model.ExchangeRates;
import com.tm.exchangeratesapp.repository.ExchangeRatesRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ExchangeRatesServiceImpl implements ExchangeRatesService{
    private final ExchangeRatesRepository repository;

    public ExchangeRatesServiceImpl(ExchangeRatesRepository repository) {
        this.repository = repository;
    }

    @Override
    public ExchangeRates save(ExchangeRates exchangeRates) {
        return repository.save(exchangeRates);
    }

    @Override
    public ExchangeRates readById(long id) {
        return null;
    }

    @Override
    public ExchangeRates getLast() {
        return repository.getLast();
    }

    @Override
    public List<ExchangeRates> findAll() {
        return repository.findAll();
    }
}
