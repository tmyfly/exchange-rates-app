package com.tm.exchangeratesapp.service;

import com.tm.exchangeratesapp.model.AverageExchangeRates;

import java.time.LocalDateTime;
import java.util.List;

public interface AverageExchangeRatesService {
    AverageExchangeRates save(AverageExchangeRates averageExchangeRates);
    AverageExchangeRates readById(long id);
    List<AverageExchangeRates> findAll();
    List<AverageExchangeRates> getLast();

    List<AverageExchangeRates> searchByCriteria(LocalDateTime startDate, LocalDateTime endDate);

}
