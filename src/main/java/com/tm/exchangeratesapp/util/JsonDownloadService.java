package com.tm.exchangeratesapp.util;

public interface JsonDownloadService {
    public String download();
}
