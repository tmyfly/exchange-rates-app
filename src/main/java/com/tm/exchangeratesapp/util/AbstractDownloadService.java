package com.tm.exchangeratesapp.util;

import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class AbstractDownloadService implements JsonDownloadService {
    private final String urlJson;

    public AbstractDownloadService(String urlJson) {
        this.urlJson = urlJson;
    }

    @Override
    public String download() {
        try {
            URL obj = new URL(urlJson);
            HttpURLConnection con = (HttpURLConnection) obj.openConnection();
            con.setRequestMethod("GET");
            con.setRequestProperty("User-Agent", "USER_AGENT");
            int responseCode = con.getResponseCode();
            if (responseCode == HttpURLConnection.HTTP_OK) {
                BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
                String inputLine;
                StringBuffer response = new StringBuffer();

                while ((inputLine = in.readLine()) != null) {
                    response.append(inputLine);
                }
                in.close();
                return response.toString();
            } else {
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "GET request did not work.");
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
