package com.tm.exchangeratesapp.app;

import com.tm.exchangeratesapp.util.JsonDownloadService;
import com.tm.exchangeratesapp.util.MonoDownloadServiceImpl;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
@PropertySource("classpath:bank-api-url.properties")
public class MonoConfiguration {
    private final String url;

    public MonoConfiguration(@Value("${mono}") String url) {
        this.url = url;
    }

    @Bean("monoBean")
    JsonDownloadService jsonDownloadService() {
        return new MonoDownloadServiceImpl(url);
    }
}
