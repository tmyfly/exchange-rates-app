package com.tm.exchangeratesapp.app;

import com.tm.exchangeratesapp.util.JsonDownloadService;
import com.tm.exchangeratesapp.util.PrivatDownloadServiceImpl;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
@PropertySource("classpath:bank-api-url.properties")
public class PrivatConfiguration {
    private final String url;

    public PrivatConfiguration(@Value("${privat}") String url) {
        this.url = url;
    }

    @Bean("privatBean")
    JsonDownloadService jsonDownloadService() {
        return new PrivatDownloadServiceImpl(url);
    }
}
