package com.tm.exchangeratesapp.app;

import com.tm.exchangeratesapp.model.AverageExchangeRates;
import com.tm.exchangeratesapp.model.CurrencyCodes;
import com.tm.exchangeratesapp.model.ExchangeRates;
import com.tm.exchangeratesapp.model.RateBank;
import com.tm.exchangeratesapp.service.AverageExchangeRatesService;
import com.tm.exchangeratesapp.service.ExchangeRatesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class RatesBankAppImpl implements RatesBankApp{
    @Autowired
    private ExchangeRatesService exchangeRatesService;

    @Autowired
    private AverageExchangeRatesService averageExchangeRatesService;

    @Autowired
    private BankFactory bankFactory;

    @Override
    public void run() {
        List<RateBank> allBank = new ArrayList<>();

        bankFactory.getStrategies().stream()
                .map(RatesBank::getListRates)
                .forEach(allBank::addAll);

        newExchangeRatesPeriod(allBank);
    }

    private void newExchangeRatesPeriod(List<RateBank> allBank) {
        ExchangeRates exchangeRates = new ExchangeRates();
        exchangeRates.getRateBankList().addAll(allBank);
        extractedAverage(exchangeRates);
        exchangeRatesService.save(exchangeRates);
    }

    private void extractedAverage(ExchangeRates exchangeRates) {
        List<String>codesList = List.of(CurrencyCodes.EUR.name(), CurrencyCodes.USD.name());
        codesList.forEach(s -> {
            AverageExchangeRates averageExchangeRates = new AverageExchangeRates();
            averageExchangeRates.setCurrency(s);
            averageExchangeRates.setAverageBuy(getAverageBay(s, exchangeRates));
            averageExchangeRates.setAverageSell(getAverageSell(s, exchangeRates));
            var average = averageExchangeRatesService.save(averageExchangeRates);
            exchangeRates.getAverageExchangeRatesList().add(average);
        });
    }

    private static double getAverageBay(String codes, ExchangeRates exchangeRates) {
        return exchangeRates.getRateBankList().stream()
                .filter(rateBank -> rateBank.getCurrency().equals(codes))
                .mapToDouble(RateBank::getRateBuy)
                .average()
                .orElse(Double.NaN);
    }

    private static double getAverageSell(String codes, ExchangeRates exchangeRates) {
        return exchangeRates.getRateBankList().stream()
                .filter(rateBank -> rateBank.getCurrency().equals(codes))
                .mapToDouble(RateBank::getRateSell)
                .average()
                .orElse(Double.NaN);
    }
}
