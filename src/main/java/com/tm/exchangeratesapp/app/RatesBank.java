package com.tm.exchangeratesapp.app;

import com.tm.exchangeratesapp.model.RateBank;

import java.util.List;

public interface RatesBank {
    List<RateBank> getListRates();

    String nameBank();
}
