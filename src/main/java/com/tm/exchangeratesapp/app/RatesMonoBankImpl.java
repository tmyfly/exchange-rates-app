package com.tm.exchangeratesapp.app;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tm.exchangeratesapp.model.CurrencyCodes;
import com.tm.exchangeratesapp.model.RateBank;
import com.tm.exchangeratesapp.model.RateMono;
import com.tm.exchangeratesapp.service.RateBankService;
import com.tm.exchangeratesapp.service.RateMonoService;
import com.tm.exchangeratesapp.util.JsonDownloadService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class RatesMonoBankImpl implements RatesBank {
    private final String nameBank = "MonoBank";

    private final JsonDownloadService jsonDownloadService;

    @Autowired
    private ObjectMapper mapper;

    private final RateMonoService rateMonoService;
    private final RateBankService rateBankService;

    public RatesMonoBankImpl(@Qualifier("monoBean") JsonDownloadService jsonDownloadService, RateMonoService rateMonoService, RateBankService rateBankService) {
        this.jsonDownloadService = jsonDownloadService;
        this.rateMonoService = rateMonoService;
        this.rateBankService = rateBankService;
    }

    @Override
    public List<RateBank> getListRates() {
        String json = jsonDownloadService.download();
        return gettingRates(json);
    }

    @Override
    public String nameBank() {
        return nameBank;
    }

    private List<RateBank> gettingRates(String json) {
        List<RateMono> listRate = null;
        if (!json.isBlank()) {
            try {
                List<RateMono> listRateTemp = mapper.readValue(json, new TypeReference<>() {
                });
                listRate = listRateTemp.stream()
                        .filter(rateMono -> ((rateMono.getCurrencyCodeA() == CurrencyCodes.EUR.getValue())
                                || (rateMono.getCurrencyCodeA() == CurrencyCodes.USD.getValue()))
                                && (rateMono.getCurrencyCodeB() == CurrencyCodes.UAH.getValue()))
                        .map(rateMonoService::save)
                        .collect(Collectors.toList());
             } catch (JsonProcessingException e) {
                throw new RuntimeException(e);
            }
        }
        assert listRate != null;
        return listRate.stream().map(rateMono -> {
            RateBank rateBank = new RateBank();
            rateBank.setNameBank(nameBank);
            rateBank.setCurrency(getCodeToKey(rateMono.getCurrencyCodeA()).name());
            rateBank.setRateBuy(rateMono.getRateBuy());
            rateBank.setRateSell(rateMono.getRateSell());
            return rateBankService.save(rateBank);
            })
                .collect(Collectors.toList());
    }

    private static CurrencyCodes getCodeToKey(int num) {
        for (var s : CurrencyCodes.values()) {
            if (s.getValue() == num) {
                return s;
            }
        }
        throw new IllegalArgumentException();
    }
}
