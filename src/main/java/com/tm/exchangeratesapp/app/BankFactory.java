package com.tm.exchangeratesapp.app;

import org.springframework.stereotype.Component;

import java.util.Set;

@Component
public class BankFactory {
    private final Set<RatesBank> strategies;

    public BankFactory(Set<RatesBank> strategySet) {
        this.strategies = strategySet;
    }

    public Set<RatesBank> getStrategies() {
        return strategies;
    }
}
