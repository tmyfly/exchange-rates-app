package com.tm.exchangeratesapp.app;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tm.exchangeratesapp.model.RateBank;
import com.tm.exchangeratesapp.model.RatePrivat;
import com.tm.exchangeratesapp.service.RateBankService;
import com.tm.exchangeratesapp.service.RatePrivatService;
import com.tm.exchangeratesapp.util.JsonDownloadService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class RatesPrivatBankImpl implements RatesBank {
    private final String nameBank = "PrivatBank";

    private final JsonDownloadService jsonDownloadService;

    @Autowired
    private ObjectMapper mapper;

    private final RatePrivatService ratePrivatService;
    private final RateBankService rateBankService;

    public RatesPrivatBankImpl(@Qualifier("privatBean") JsonDownloadService jsonDownloadService,
                               RatePrivatService ratePrivatService, RateBankService rateBankService) {
        this.jsonDownloadService = jsonDownloadService;
        this.ratePrivatService = ratePrivatService;
        this.rateBankService = rateBankService;
    }

    @Override
    public List<RateBank> getListRates() {
        String json = jsonDownloadService.download();
        return gettingRates(json);
    }

    @Override
    public String nameBank() {
        return null;
    }

    private List<RateBank> gettingRates(String json) {
        List<RatePrivat> listRate = null;
        if (!json.isBlank()) {
            try {
                List<RatePrivat> listRateTemp = mapper.readValue(json, new TypeReference<>() {
                });
                listRate = listRateTemp.stream()
                        .map(ratePrivatService::save)
                        .collect(Collectors.toList());
             } catch (JsonProcessingException e) {
                throw new RuntimeException(e);
            }
        }
        assert listRate != null;
        return listRate.stream().map(ratePrivat -> {
            RateBank rateBank = new RateBank();
            rateBank.setNameBank(nameBank);
            rateBank.setCurrency(ratePrivat.getCcy());
            rateBank.setRateBuy(ratePrivat.getBuy());
            rateBank.setRateSell(ratePrivat.getSale());
            return rateBankService.save(rateBank);
            })
                .collect(Collectors.toList());
    }
}
