package com.tm.exchangeratesapp.app;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.tm.exchangeratesapp.model.CurrencyCodes;
import com.tm.exchangeratesapp.model.RateBank;
import com.tm.exchangeratesapp.model.RateNbu;
import com.tm.exchangeratesapp.service.RateBankService;
import com.tm.exchangeratesapp.service.RateNbuService;
import com.tm.exchangeratesapp.util.JsonDownloadService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class RatesNbuBankImpl implements RatesBank {
    private final String nameBank = "NBU";

    private final JsonDownloadService jsonDownloadService;

    @Autowired
    private ObjectMapper mapper;

    private final RateNbuService rateNbuService;
    private final RateBankService rateBankService;

    public RatesNbuBankImpl(@Qualifier("nbuBean") JsonDownloadService jsonDownloadService,
                            RateNbuService rateNbuService, RateBankService rateBankService) {
        this.jsonDownloadService = jsonDownloadService;
        this.rateNbuService = rateNbuService;
        this.rateBankService = rateBankService;
    }

    @Override
    public List<RateBank> getListRates() {
        String json = jsonDownloadService.download();
        return gettingRates(json);
    }

    @Override
    public String nameBank() {
        return nameBank;
    }

    private List<RateBank> gettingRates(String json) {
        mapper.registerModule(new JavaTimeModule());

        List<RateNbu> listRate = null;
        if (!json.isBlank()) {
            try {
                List<RateNbu> listRateTemp = mapper.readValue(json, new TypeReference<>() {
                });
                listRate = listRateTemp.stream()
                        .filter(rateNbu -> ((rateNbu.getCc().equals(CurrencyCodes.EUR.name()))
                                || (rateNbu.getCc().equals(CurrencyCodes.USD.name()))))
                        .map(rateNbuService::save)
                        .collect(Collectors.toList());
             } catch (JsonProcessingException e) {
                throw new RuntimeException(e);
            }
        }
        assert listRate != null;
        return listRate.stream()
                .map(rateNbu -> {
                    RateBank rateBank = new RateBank();
                    rateBank.setNameBank("NBU");
                    rateBank.setCurrency(rateNbu.getCc());
                    rateBank.setRateBuy(rateNbu.getRate());
                    rateBank.setRateSell(rateNbu.getRate());
                    return rateBankService.save(rateBank);
                })
                .collect(Collectors.toList());
    }
}
