package com.tm.exchangeratesapp.app;

import com.tm.exchangeratesapp.util.JsonDownloadService;
import com.tm.exchangeratesapp.util.NbuDownloadServiceImpl;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
@PropertySource("classpath:bank-api-url.properties")
public class NbuConfiguration {
    private final String url;

    public NbuConfiguration(@Value("${nbu}") String url) {
        this.url = url;
    }

    @Bean("nbuBean")
    JsonDownloadService jsonDownloadService() {
        return new NbuDownloadServiceImpl(url);
    }
}
