package com.tm.exchangeratesapp.config;

import com.tm.exchangeratesapp.app.RatesBankApp;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableScheduling
public class AppConfiguration {

    @Autowired
    private RatesBankApp ratesBank;

    @Scheduled(fixedDelay = 600000) // 10 min
    public void scheduleTask() {
        ratesBank.run();

        System.out.println( "Fixed delay task - " + System.currentTimeMillis() / 1000);
    }

    @Bean
    public Docket api() {
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.tm.exchangeratesapp"))
                .build();
    }
}
