package com.tm.exchangeratesapp.repository;

import com.tm.exchangeratesapp.model.RatePrivat;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RatePrivatRepository extends JpaRepository<RatePrivat, Long> {
}
