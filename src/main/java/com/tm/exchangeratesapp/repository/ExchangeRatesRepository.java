package com.tm.exchangeratesapp.repository;

import com.tm.exchangeratesapp.model.ExchangeRates;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;


public interface ExchangeRatesRepository extends JpaRepository<ExchangeRates, Long> {
    @Query(value = "SELECT * FROM exchange_rates ORDER BY id DESC LIMIT 1", nativeQuery = true)
    ExchangeRates getLast();
}
