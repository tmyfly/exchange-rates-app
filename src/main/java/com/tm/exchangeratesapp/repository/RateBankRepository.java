package com.tm.exchangeratesapp.repository;

import com.tm.exchangeratesapp.model.RateBank;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RateBankRepository extends JpaRepository<RateBank, Long> {
}
