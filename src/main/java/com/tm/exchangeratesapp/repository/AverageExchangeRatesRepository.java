package com.tm.exchangeratesapp.repository;

import com.tm.exchangeratesapp.model.AverageExchangeRates;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDateTime;
import java.util.List;

public interface AverageExchangeRatesRepository extends JpaRepository<AverageExchangeRates, Long> {
    List<AverageExchangeRates> getAverageExchangeRatesByTimeBetween(LocalDateTime time, LocalDateTime time1);

}
