package com.tm.exchangeratesapp.repository;

import com.tm.exchangeratesapp.model.RateNbu;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RateNbuRepository extends JpaRepository<RateNbu, Long> {
}
