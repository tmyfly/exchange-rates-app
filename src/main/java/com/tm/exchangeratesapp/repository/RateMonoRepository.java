package com.tm.exchangeratesapp.repository;

import com.tm.exchangeratesapp.model.RateMono;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface RateMonoRepository extends JpaRepository<RateMono, Long> {

    Optional<RateMono> findRateMonoByCurrencyCodeAAndCurrencyCodeBAndDate(int currA, int currB, int date);
}
